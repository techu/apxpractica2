package com.bbva.qwai.batch.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="TQWAICUS", schema="QWAI")
public class CustomerEntity {
    
    @EmbeddedId
    private CustomerEntityPK id;
    
    @Column(name = "DES_NOMBFJ")
    private String firstName;
    
    @Column(name = "DES_APELLUNO")
    private String lastName;
    
    @Column(name = "DES_APELLDOS")
    private String secondLastName;
    
    @Column(name = "COD_TRATANOR")
    private String personalTitle;
    
    @Column(name = "XTI_SEXO")
    private String genderId;
    
    @Column(name = "COD_TIPIDENT")
    private String identityDocumentType;
    
    @Column(name = "COD_DOCUMPS")
    private String identityDocumentNumber;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "FEC_FNACIF")
    private Date birthDate;
    
    @Column(name = "COD_CECIVI")
    private String maritalStatus;
    
    @Column(name = "AUD_USUARIO")
    private String audUser;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "AUD_FMMODIF")
    private Date audFxModif;

    public CustomerEntityPK getId() {
   	 return id;
    }

    public void setId(CustomerEntityPK id) {
   	 this.id = id;
    }

    public String getFirstName() {
   	 return firstName;
    }

    public void setFirstName(String firstName) {
   	 this.firstName = firstName;
    }

    public String getLastName() {
   	 return lastName;
    }

    public void setLastName(String lastName) {
   	 this.lastName = lastName;
    }
    

    public String getSecondLastName() {
   	 return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
   	 this.secondLastName = secondLastName;
    }

    public String getPersonalTitle() {
   	 return personalTitle;
    }

    public void setPersonalTitle(String personalTitle) {
   	 this.personalTitle = personalTitle;
    }

    public String getGenderId() {
   	 return genderId;
    }

    public void setGenderId(String genderId) {
   	 this.genderId = genderId;
    }

    public String getIdentityDocumentType() {
   	 return identityDocumentType;
    }

    public void setIdentityDocumentType(String identityDocumentType) {
   	 this.identityDocumentType = identityDocumentType;
    }

    public String getIdentityDocumentNumber() {
   	 return identityDocumentNumber;
    }

    public void setIdentityDocumentNumber(String identityDocumentNumber) {
   	 this.identityDocumentNumber = identityDocumentNumber;
    }

    public Date getBirthDate() {
   	 return birthDate;
    }

    public void setBirthDate(Date birthDate) {
   	 this.birthDate = birthDate;
    }

    public String getMaritalStatus() {
   	 return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
   	 this.maritalStatus = maritalStatus;
    }

    public String getAudUser() {
   	 return audUser;
    }

    public void setAudUser(String audUser) {
   	 this.audUser = audUser;
    }
    
    public Date getAudFxModif() {
   	 return audFxModif;
    }

    public void setAudFxModif(Date audFxModif) {
   	 this.audFxModif = audFxModif;
    }

    @Override
    public String toString() {
   	 return "CustomerEntity [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", secondLastName="
   			 + secondLastName + ", personalTitle=" + personalTitle + ", genderId=" + genderId
   			 + ", identityDocumentType=" + identityDocumentType + ", identityDocumentNumber="
   			 + identityDocumentNumber + ", birthDate=" + birthDate + ", maritalStatus=" + maritalStatus
   			 + ", audUser=" + audUser + ", audFxModif=" + audFxModif + "]";
    }

    @Override
    public int hashCode() {
   	 final int prime = 31;
   	 int result = 1;
   	 result = prime * result + ((audFxModif == null) ? 0 : audFxModif.hashCode());
   	 result = prime * result + ((audUser == null) ? 0 : audUser.hashCode());
   	 result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
   	 result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
   	 result = prime * result + ((genderId == null) ? 0 : genderId.hashCode());
   	 result = prime * result + ((id == null) ? 0 : id.hashCode());
   	 result = prime * result + ((identityDocumentNumber == null) ? 0 : identityDocumentNumber.hashCode());
   	 result = prime * result + ((identityDocumentType == null) ? 0 : identityDocumentType.hashCode());
   	 result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
   	 result = prime * result + ((maritalStatus == null) ? 0 : maritalStatus.hashCode());
   	 result = prime * result + ((personalTitle == null) ? 0 : personalTitle.hashCode());
   	 result = prime * result + ((secondLastName == null) ? 0 : secondLastName.hashCode());
   	 return result;
    }

    @Override
    public boolean equals(Object obj) {
   	 if (this == obj)
   		 return true;
   	 if (obj == null)
   		 return false;
   	 if (getClass() != obj.getClass())
   		 return false;
   	 CustomerEntity other = (CustomerEntity) obj;
   	 if (audFxModif == null) {
   		 if (other.audFxModif != null)
   			 return false;
   	 } else if (!audFxModif.equals(other.audFxModif))
   		 return false;
   	 if (audUser == null) {
   		 if (other.audUser != null)
   			 return false;
   	 } else if (!audUser.equals(other.audUser))
   		 return false;
   	 if (birthDate == null) {
   		 if (other.birthDate != null)
   			 return false;
   	 } else if (!birthDate.equals(other.birthDate))
   		 return false;
   	 if (firstName == null) {
   		 if (other.firstName != null)
   			 return false;
   	 } else if (!firstName.equals(other.firstName))
   		 return false;
   	 if (genderId == null) {
   		 if (other.genderId != null)
   			 return false;
   	 } else if (!genderId.equals(other.genderId))
   		 return false;
   	 if (id == null) {
   		 if (other.id != null)
   			 return false;
   	 } else if (!id.equals(other.id))
   		 return false;
   	 if (identityDocumentNumber == null) {
   		 if (other.identityDocumentNumber != null)
   			 return false;
   	 } else if (!identityDocumentNumber.equals(other.identityDocumentNumber))
   		 return false;
   	 if (identityDocumentType == null) {
   		 if (other.identityDocumentType != null)
   			 return false;
   	 } else if (!identityDocumentType.equals(other.identityDocumentType))
   		 return false;
   	 if (lastName == null) {
   		 if (other.lastName != null)
   			 return false;
   	 } else if (!lastName.equals(other.lastName))
   		 return false;
   	 if (maritalStatus == null) {
   		 if (other.maritalStatus != null)
   			 return false;
   	 } else if (!maritalStatus.equals(other.maritalStatus))
   		 return false;
   	 if (personalTitle == null) {
   		 if (other.personalTitle != null)
   			 return false;
   	 } else if (!personalTitle.equals(other.personalTitle))
   		 return false;
   	 if (secondLastName == null) {
   		 if (other.secondLastName != null)
   			 return false;
   	 } else if (!secondLastName.equals(other.secondLastName))
   		 return false;
   	 return true;
    }

}



