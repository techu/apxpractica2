package com.bbva.qwai.batch.processor;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Map;

import org.springframework.batch.item.ItemProcessor;

import com.bbva.qwai.batch.entity.CustomerEntity;
import com.bbva.qwai.batch.entity.CustomerEntityPK;
import com.bbva.qwai.dto.domain.CustomerDTO;

public class CustomerItemProcessor implements ItemProcessor<CustomerDTO, CustomerEntity> {

    private static String TEAM_ID = "TEAM 1";
    
    @Override
    public CustomerEntity process(CustomerDTO customerDTO) throws Exception {
    
   	 CustomerEntityPK customerEntityPK = new CustomerEntityPK();
   	 customerEntityPK.setNationality(customerDTO.getNationality());
   	 customerEntityPK.setCodEntity(customerDTO.getEntityCode());
   	 customerEntityPK.setCustomerId(customerDTO.getCustomerId());
   	 
   	 CustomerEntity customerEntity = new CustomerEntity();
   	 customerEntity.setId(customerEntityPK);
   	 customerEntity.setFirstName(customerDTO.getFirstName());
   	 
   	 String[] surnames = customerDTO.getLastName().split(" ");
   	 customerEntity.setLastName(surnames[0].trim());
   	 customerEntity.setSecondLastName(surnames[1].trim());
   	 
   	 customerEntity.setBirthDate(customerDTO.getBirthDate());
   	 customerEntity.setGenderId(customerDTO.getGenderId());
   	 customerEntity.setIdentityDocumentType(customerDTO.getIdentityDocumentType());
   	 customerEntity.setIdentityDocumentNumber(customerDTO.getIdentityDocumentNumber());
   	 customerEntity.setPersonalTitle(customerDTO.getPersonalTitle());
   	 customerEntity.setMaritalStatus(customerDTO.getMaritalStatus());
   	 
   	 customerEntity.setAudUser(TEAM_ID);
   	 customerEntity.setAudFxModif(Date.valueOf(LocalDate.now()));
   	 
   	 return customerEntity;
    }

}


